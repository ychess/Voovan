package org.voovan.http;

/**
 * 类文字命名
 *
 * @author: helyho
 * DBase Framework.
 * WebSite: https://github.com/helyho/DBase
 * Licence: Apache v2 License
 */
public enum  HttpSessionParam {
    TYPE, HTTP_REQUEST, HTTP_RESPONSE, KEEP_ALIVE, KEEP_ALIVE_TIMEOUT
}
